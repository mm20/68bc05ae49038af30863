import colorsys
import threading
from time import sleep

import numpy
import pigpio

import colour
import util


class Light:
    def __init__(self, id, rgpio, ggpio, bgpio, pi, gamma=2.2, name='Lamp ' + str(id), model='LST002'):
        self.pi = pi
        self.id = id
        self.rgpio = rgpio
        self.ggpio = ggpio
        self.bgpio = bgpio
        self.gamma = gamma
        self.effect = 'none'
        self.fadeStopFlag = False
        self.fadeThread = None
        self.effectStopFlag = False
        self.effectThread = None
        self.state = False
        self.modelid = model
        self.model = 'Extended color light'
        self.name = name
        self.pwm_max = self.pi.get_PWM_range(rgpio)
        self.mode = 'ct'
        self.pi.set_mode(self.rgpio, pigpio.OUTPUT)
        self.pi.set_mode(self.ggpio, pigpio.OUTPUT)
        self.pi.set_mode(self.bgpio, pigpio.OUTPUT)
        self.pi.set_PWM_dutycycle(self.rgpio, 0)
        self.pi.set_PWM_dutycycle(self.ggpio, 0)
        self.pi.set_PWM_dutycycle(self.bgpio, 0)
        self.bri = 254
        self.hue = 0
        self.sat = 0
        # Red: 0.0 - 1.0
        self.red = 1
        # Green: 0.0 - 1.0
        self.green = 1
        # Blue: 0.0 - 1.0
        self.blue = 1
        self.setCT(250)
        self.xy = self.getXY()

    def setRGB(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

        hsv = colorsys.rgb_to_hsv(red, green, blue)
        self.hue = int(hsv[0] * 65535)
        self.sat = int(hsv[1] * 254)
        self.bri = int(hsv[2] * 254)
        self.updateColor()

    def setBri(self, bri):
        self.bri = bri
        rgb = colorsys.hsv_to_rgb(self.hue / 65535.0, self.sat / 254.0, bri / 254.0)
        self.red = rgb[0]
        self.green = rgb[1]
        self.blue = rgb[2]
        self.updateColor()

    def setSat(self, sat):
        self.sat = sat
        rgb = colorsys.hsv_to_rgb(self.hue / 65535.0, sat / 254.0, self.bri / 254.0)
        self.red = rgb[0]
        self.green = rgb[1]
        self.blue = rgb[2]
        self.updateColor()

    def setHue(self, hue):
        self.hue = hue
        rgb = colorsys.hsv_to_rgb(hue / 65535.0, self.sat / 254.0, self.bri / 254.0)
        self.red = rgb[0]
        self.green = rgb[1]
        self.blue = rgb[2]
        self.updateColor()

    def setState(self, state):
        self.state = state
        self.updateColor()

    def gammaCorrect(self, value):
        val = self.pwm_max * pow(value, self.gamma)
        #return 1 if val < 1 and value > 0 else val
        return val

    def reverseGammaCorrect(self, value):
        return pow(value / self.pwm_max, 1 / self.gamma)

    def getXY(self):
        red = self.red
        green = self.green
        blue = self.blue
        rgb = numpy.array([red, green, blue])
        XYZ = colour.sRGB_to_XYZ(rgb)
        xy = colour.XYZ_to_xy(XYZ)
        return [xy[0], xy[1]]

    def setXY(self, x, y):
        self.xy = [x, y]
        xy = numpy.array([x, y])
        xyY = colour.xy_to_xyY(xy, self.bri / 254)
        XYZ = colour.xyY_to_XYZ(xyY)
        sRGB = colour.XYZ_to_sRGB(XYZ)
        mn = min(sRGB)
        if (mn < 0):
            sRGB[0] = sRGB[0] - mn
            sRGB[1] = sRGB[1] - mn
            sRGB[2] = sRGB[2] - mn
        mx = max(sRGB)
        if mx > 1:
            sRGB[0] = sRGB[0] / mx
            sRGB[1] = sRGB[1] / mx
            sRGB[2] = sRGB[2] / mx

        self.setRGB(sRGB[0], sRGB[1], sRGB[2])

    def setCT(self, ct):
        self.ct = ct
        xy = colour.CCT_to_xy(1000000 / ct)
        self.xy = [xy[0],xy[1]]
        xyY = colour.xy_to_xyY(xy, self.bri / 254)
        XYZ = colour.xyY_to_XYZ(xyY)
        sRGB = colour.XYZ_to_sRGB(XYZ)
        mn = min(sRGB)
        if (mn < 0):
            sRGB[0] = sRGB[0] - mn
            sRGB[1] = sRGB[1] - mn
            sRGB[2] = sRGB[2] - mn
        mx = max(sRGB)
        if mx > 1:
            sRGB[0] = sRGB[0] / mx
            sRGB[1] = sRGB[1] / mx
            sRGB[2] = sRGB[2] / mx
        self.setRGB(sRGB[0], sRGB[1], sRGB[2])

    def setEffect(self, effect):
        if self.effectThread != None and self.effectThread.is_alive():
            self.effectStopFlag = True
            self.effectThread.join()
        self.effectStopFlag = False
        self.effect = effect
        if 'colorloop' == effect:
            self.effectThread = threading.Thread(target=self.colorloop, args=())
            self.effectThread.start()

    def getRGB(self):
        return 'rgb(' + str(int(self.red * 255)) + ',' + str(int(self.green * 255)) + ',' + str(
            int(self.blue * 255)) + ')'

    def updateColor(self, transitiontime=4):
        if not self.state:
            if (self.fadeThread != None and self.fadeThread.is_alive()):
                self.fadeStopFlag = True
                self.fadeThread.join()
            r = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.rgpio))
            g = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.ggpio))
            b = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.bgpio))
            self.fadeThread = threading.Thread(target=self.fade, args=(r, g, b, 0, 0, 0, transitiontime * 100))
            self.fadeThread.start()
        else:
            if (self.fadeThread != None and self.fadeThread.is_alive()):
                self.fadeStopFlag = True
                self.fadeThread.join()
            r = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.rgpio))
            g = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.ggpio))
            b = self.reverseGammaCorrect(self.pi.get_PWM_dutycycle(self.bgpio))
            self.fadeThread = threading.Thread(target=self.fade, args=(r, g, b, self.red, self.green, self.blue,
                                                                       transitiontime * 100))
            self.fadeThread.start()

    def apply(self, request, resp):
        if 'effect' in request:
            self.setEffect(request['effect'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/effect'] = self.effect
            resp.append(result)
        else:
            self.setEffect('none')
        if 'name' in request:
            self.name = request['name']
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/name'] = self.name
            resp.append(result)
        if 'on' in request:
            self.setState(request['on'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/on'] = self.state
            resp.append(result)
        if 'bri' in request:
            self.setBri(request['bri'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/bri'] = self.bri
            resp.append(result)
        if 'sat' in request:
            self.mode = 'hs'
            self.setSat(request['sat'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/sat'] = self.sat
            resp.append(result)
        if 'hue_inc' in request:
            self.mode = 'hs'
            hue = request['hue_inc'] + self.hue
            if hue > 65535:
                hue = hue + 65535
            if hue < 0:
                hue = hue - 65535
            self.setHue(hue)
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/hue'] = self.hue
            resp.append(result)
        if 'hue' in request:
            self.mode = 'hs'
            self.setHue(request['hue'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/hue'] = self.hue
            resp.append(result)
        if 'xy' in request:
            self.mode = 'xy'
            self.setXY(request['xy'][0], request['xy'][1])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/xy'] = self.getXY()
            resp.append(result)
        if 'ct' in request:
            self.mode = 'ct'
            self.setCT(request['ct'])
            result = {'success': {}}
            result['success']['/lights/' + str(self.id) + '/state/ct'] = self.ct
            resp.append(result)
        if 'transitiontime' in request:
            self.updateColor(transitiontime=request['transitiontime'])

        return resp

    def pack(self):
        light = {}
        light['state'] = {}
        light['state']['on'] = self.state
        light['state']['hue'] = self.hue
        light['state']['sat'] = self.sat
        light['state']['bri'] = self.bri
        light['state']['xy'] = self.xy
        light['state']['ct'] = self.ct
        light['state']['alert'] = 'select'
        light['state']['effect'] = self.effect
        light['state']['colormode'] = self.mode
        light['state']['reachable'] = True
        light['type'] = self.model
        light['name'] = self.name
        light['modelid'] = self.modelid
        light['swversion'] = '66009461'
        light['uniqueid'] = util.getMacAddress() + '-' + str(self.id)
        light['manufacturername'] = 'Philips'
        light['luminaireuniqueid'] = None
        light['pointsymbol'] = {}
        light['pointsymbol']['1'] = 'none'
        light['pointsymbol']['2'] = 'none'
        light['pointsymbol']['3'] = 'none'
        light['pointsymbol']['4'] = 'none'
        light['pointsymbol']['5'] = 'none'
        light['pointsymbol']['6'] = 'none'
        light['pointsymbol']['7'] = 'none'
        light['pointsymbol']['8'] = 'none'
        return light

    def luminaireuniqueid(self):
        id = str(self.id)
        return id + id + ':' + id + id + ':' + id + id + ':' + id + id + ':' + id + id + ':' + id + id

    def fade(self, r1, g1, b1, r2, g2, b2, time=400):
        diffr = r2 - r1
        diffg = g2 - g1
        diffb = b2 - b1
        steps = int(time / 10)
        for i in range(1, steps):
            if self.fadeStopFlag:
                self.fadeStopFlag = False
                return
            r = r1 + (i / steps) * diffr
            g = g1 + (i / steps) * diffg
            b = b1 + (i / steps) * diffb
            self.pi.set_PWM_dutycycle(self.rgpio, int(self.gammaCorrect(r)))
            self.pi.set_PWM_dutycycle(self.ggpio, int(self.gammaCorrect(g)))
            self.pi.set_PWM_dutycycle(self.bgpio, int(self.gammaCorrect(b)))
            sleep(time / 100000)
        self.pi.set_PWM_dutycycle(self.rgpio, int(self.gammaCorrect(r2)))
        self.pi.set_PWM_dutycycle(self.ggpio, int(self.gammaCorrect(g2)))
        self.pi.set_PWM_dutycycle(self.bgpio, int(self.gammaCorrect(b2)))
        self.fadeStopFlag = False

    def colorloop(self):
        self.setSat(254)
        while not self.effectStopFlag:
            for hue in range(0, 65535):
                if self.effectStopFlag:
                    break
                self.setHue(hue)
                sleep(0.01)
        self.effectStopFlag = False
