import globals


class Group:
    def __init__(self, id, lights, name='Room ' + str(id), class_='Other', type_='Room'):
        self.lights = lights
        self.id = id
        self.name = name
        self.class_ = class_
        self.type_ = type_

    def pack(self):
        group = {}
        group['name'] = self.name
        group['lights'] = []
        num_on = self.count_on()
        for l in self.lights:
            group['lights'].append(l)
        group['type'] = "LightGroup"
        group['action'] = {}
        if len(self.lights) > 0:
            lamp = globals.lights[self.lights[0]]
            group['action']['on'] = lamp.state
            group['action']['bri'] = lamp.bri
            group['action']['hue'] = lamp.hue
            group['action']['sat'] = lamp.sat
            group['action']['effect'] = 'none'
            group['action']['xy'] = lamp.xy
            group['action']['ct'] = lamp.ct
        group['state'] = {}
        group['state']['any_on'] = num_on >= 1
        group['state']['all_on'] = num_on == len(self.lights) and num_on > 0
        group['type'] = self.type_
        group['class'] = self.class_
        group['name'] = self.name
        group['uniqueid'] = None
        return group

    def count_on(self):
        num_on = 0
        for l in self.lights:
            lamp = globals.lights[l]
            if lamp.state:
                num_on += 1
        return num_on

    def apply(self, request, resp):
        if 'name' in request:
            self.name = request['name']
            result = {}
            result['success'] = {}
            result['success']['/groups/' + str(self.id) + '/name'] = self.name
            resp.append(result)
        if 'class' in request:
            self.name = request['class']
            result = {}
            result['success'] = {}
            result['success']['/groups/' + str(self.id) + '/class'] = self.class_
            resp.append(result)
        if 'lights' in request and len(request['lights']) > 0:
            self.lights.clear()
            self.lights = []
            for l in request['lights']:
                self.lights.append(l)
                result['success'] = {}
                result['success']['/groups/' + str(self.id) + '/lights'] = []
            for l in self.lights:
                result['success']['/groups/' + str(self.id) + '/lights'].append(l)
            resp.append(result)
        if 'scene' in request and request['scene'] in globals.scenes:
            globals.scenes[request['scene']].recall()
            result = {'success': {}}
            result['success']['/groups/' + str(self.id) + '/scene'] = request['scene']
            resp.append(result)
        return resp
