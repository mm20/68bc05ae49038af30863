#!/usr/bin/env python
import json
import os
import pickle
import socket
from datetime import datetime
from json.decoder import JSONDecodeError
from time import sleep

import pytz
from flask import Flask, Response, request, logging, render_template

import globals
import util
from application import Application
from group import Group
from scene import Scene

app = Flask(__name__)


# applications = []
# lamps = []
# groups = []


@app.route('/description.xml', methods=['GET'])
def description():
    return Response("""<?xml version="1.0"?>
<root xmlns="urn:schemas-upnp-org:device-1-0">
  <specVersion>
    <major>1</major>
    <minor>0</minor>
  </specVersion>
  <URLBase>""" + util.getIp() + """:80/</URLBase>
  <device>
    <deviceType>urn:schemas-upnp-org:device:Basic:1</deviceType>
    <friendlyName>Philips hue (""" + util.getIp() + """)</friendlyName>
    <manufacturer>Royal Philips Electronics</manufacturer>
    <manufacturerURL>http://www.philips.com</manufacturerURL>
    <modelDescription>Philips hue Personal Wireless Lighting</modelDescription>
    <modelName>Philips hue bridge 2012</modelName>
    <modelNumber>929000226503</modelNumber>
    <modelURL>http://www.meethue.com</modelURL>
    <serialNumber>001788102201</serialNumber>
    <UDN>uuid:2f402f80-da50-11e1-9b23-001788102201</UDN>
    <presentationURL>index.html</presentationURL>
    <iconList>
      <icon>
        <mimetype>image/png</mimetype>
        <height>48</height>
        <width>48</width>
        <depth>24</depth>
        <url>static/hue_logo_0.png</url>
      </icon>
      <icon>
        <mimetype>image/png</mimetype>
        <height>120</height>
        <width>120</width>
        <depth>24</depth>
        <url>static/hue_logo_3.png</url>
      </icon>
    </iconList>
  </device>
</root>""", mimetype="text/xml")


@app.route('/api/nupnp', methods=['GET'])
def apiNupnp():
    resp = []
    resp.append({})
    resp[0]['id'] = globals.config.id
    resp[0]['internalipaddress'] = util.getIp()
    resp[0]['macaddress'] = util.getMacAddress()
    resp[0]['name'] = 'Philips Hue'
    return json_response(resp)


def saveApplications():
    fw = open(globals.config.config_dir + 'applications', 'wb')
    pickle.dump(globals.applications, fw)
    fw.close()


def loadApplications():
    if os.path.isfile('applications'):
        fd = open(globals.config.config_dir + 'applications', 'rb')
        globals.applications = pickle.load(fd)


@app.route('/api/', methods=['POST'])
@app.route('/api', methods=['POST'])
def api():
    if not globals.config.linkbutton:
        return error(101, 'link button not pressed', '/')
    requ = json.loads(request.get_data())
    username = bytearray(os.urandom(15)).hex()
    if 'devicetype' in requ:
        devicetype = requ['devicetype']
    else:
        devicetype = username
    globals.applications[username] = Application(username, devicetype)
    saveApplications()
    resp = [{}]
    resp[0]['success'] = {}
    resp[0]['success']['username'] = username
    return json_response(resp)


@app.route('/api/config', methods=['GET', 'PUT'], defaults={'appid': None})
@app.route('/api/<appid>/config', methods=['GET', 'PUT'])
def apiAppConfig(appid):
    if request.method == 'GET':
        if not authorized(appid):
            resp = {}
            resp['swversion'] = globals.config.swversion
            resp['apiversion'] = globals.config.apiversion
            resp['name'] = 'Philips hue'
            resp['mac'] = util.getMacAddress()
            resp['linkbutton'] = True
            return json_response(resp)
        else:
            resp = globals.config.pack()
            return json_response(resp)
    else:
        resp = []
        requestData = json.loads(request.get_data().decode())
        print(json.dumps(requestData))
        return json_response(resp)


def authorized(appid):
    if appid in globals.applications:
        globals.applications[appid].last_use = util.timeToStr(datetime.now(tz=pytz.UTC))
        return True
    return False


@app.route('/api/<appid>/', methods=['GET'])
@app.route('/api/<appid>', methods=['GET'])
def apiApp(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/')
    resp = {}
    resp['lights'] = {}
    for k, v in globals.lights.items():
        resp['lights'][k] = v.pack()
    resp['groups'] = {}
    for k, v in globals.groups.items():
        if k != "0":
            resp['groups'][k] = v.pack()
    resp['scenes'] = {}
    for k, v in globals.scenes.items():
        resp['scenes'][k] = v.pack()
    resp['config'] = globals.config.pack()
    resp['schedules'] = {}
    return json_response(resp)


@app.route('/api/<appid>/lights', methods=['GET', 'POST'])
def apiAppLights(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/lights')
    if request.method == 'GET':
        resp = {}
        for k, v in globals.lights.items():
            resp[k] = v.pack()
        return json_response(resp)
    else:
        resp = [{}]
        resp[0]['success'] = {}
        resp[0]['success']['/lights'] = 'Searching for new devices'
        return json_response(resp)


@app.route('/api/<appid>/lights/new', methods=['GET'])
def apiAppLightsNew(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/lights/new')
    resp = {}
    resp['lastscan'] = 'none'
    return json_response(resp)


@app.route('/api/<appid>/lights/<id>', methods=['GET', 'PUT'])
def apiAppLightsId(appid, id):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/lights/' + id)
    if request.method == 'GET':
        if id in globals.lights:
            resp = globals.lights[id].pack()
            return json_response(resp)
        else:
            return error(3, 'resource, /lights/' + id + ' not available', '/lights/' + id)
    else:
        if id in globals.lights:
            resp = []
            try:
                requestData = json.loads(request.get_data().decode())
                resp = globals.lights[id].apply(requestData, resp)
                return json_response(resp)
            except JSONDecodeError:
                return error(2, 'body contains invalid JSON', '/lights/' + id)
        else:
            return error(3, 'resource, /lights/' + id + ' not available', '/lights/' + id)


@app.route('/api/<appid>/lights/<id>/state', methods=['PUT'])
def apiAppLightsIdState(appid, id):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/lights/' + id + '/state')
    if not id in globals.lights:
        return error(3, 'resource, /lights/' + id + ' not available', '/lights/' + id + '/state')
    resp = []
    try:
        requestData = json.loads(request.get_data().decode())
        print(json.dumps(requestData))
        resp = globals.lights[id].apply(requestData, resp)
        return json_response(resp)
    except JSONDecodeError:
        return error(2, 'body contains invalid JSON', '/lights/' + id + '/state')


@app.route('/api/<appid>/groups', methods=['GET', 'POST'])
def apiAppGroups(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/groups')
    if request.method == 'GET':
        resp = {}
        for k, v in globals.groups.items():
            if not k == '0':
                resp[k] = v.pack()
        return json_response(resp)
    else:
        print(request.get_data())
        try:
            requ = json.loads(request.get_data())
            name = requ['name'] if 'name' in requ else 'Room'
            type_ = requ['type'] if 'type' in requ else 'Room'
            class_ = requ['class'] if 'class' in requ else 'Other'
            lights = requ['lights'] if 'lights' in requ else []
            id = len(globals.groups)
            print(name + ' ' + type_ + ' ' + str(lights))
            globals.groups[str(id)] = Group(id, lights, name=name, class_=class_, type_=type_)
            resp = [{}]
            resp[0]['success'] = {}
            resp[0]['success']['id'] = str(id)
            return json_response(resp)
        except JSONDecodeError:
            return error(2, 'body contains invalid JSON', '/groups')


@app.route('/api/<appid>/groups/<id>/action', methods=['PUT'])
def apiAppGroupsIdAction(appid, id):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/groups/' + id + '/action')
    try:
        group = globals.groups[id]
        resp = []
        requestData = json.loads(request.get_data().decode())
        print(json.dumps(requestData))
        for lamp in group.lights:
            resp = globals.lights[lamp].apply(requestData, resp)

        return json_response(resp)
    except JSONDecodeError:
        return error(2, 'body contains invalid JSON', '/groups/' + id + '/action')


@app.route('/api/<appid>/groups/<id>', methods=['GET', 'PUT'])
def apiAppGroupsId(appid, id):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/groups/' + id)
    if request.method == 'GET':
        if id in globals.groups:
            resp = globals.groups[id].pack()
            return json_response(resp)
        else:
            return error(3, 'resource, /scenes/' + id + ', not available', '/groups/' + id)
    else:
        try:
            requestData = json.loads(request.get_data().decode())
            resp = []
            resp = globals.groups[int(id)].apply(requestData, resp)
            return json_response(resp)
        except JSONDecodeError:
            return error(2, 'body contains invalid JSON', '/groups/' + id)


@app.route('/api/<appid>/scenes', methods=['GET', 'POST'])
def apiAppScenes(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/scenes')
    if (request.method == 'GET'):
        resp = {}
        for k, v in globals.scenes.items():
            resp[k] = v.pack()
        return json_response(resp)
    else:
        try:
            requ = json.loads(request.get_data())
            id = bytearray(os.urandom(9)).hex()
            name = requ['name'] if 'name' in requ else id
            lights = []
            if 'lights' in requ:
                for l in requ['lights']:
                    lights.append(l)
            recycle = requ['recycle'] if 'recycle' in requ else False
            transstime = requ['transitiontime'] if 'transitiontime' in requ else 4
            locked = requ['locked'] if 'locked' in requ else False
            picture = requ['picture'] if 'picture' in requ else ''
            appdata = ''
            appdatav = 1
            if 'appdata' in requ:
                appdata = requ['appdata']['data'] if 'data' in requ['appdata'] else ''
                appdatav = requ['appdata']['version'] if 'version' in requ['appdata'] else 1
            print(request.get_data())
            globals.scenes[id] = Scene(id, name, lights, appid, appdata, appdatav, recycle, locked, picture, 2,
                                       transstime)
            resp = [{}]
            resp[0]['success'] = {}
            resp[0]['success']['id'] = id
            return json_response(resp)
        except JSONDecodeError:
            return error(2, 'body contains invalid JSON', '/scenes')


@app.route('/api/<appid>/scenes/<id>', methods=['GET'])
def apiAppScenesId(appid, id):
    if id in globals.scenes:
        resp = globals.scenes[id].pack()
        return json_response(resp)
    else:
        return error(3, 'resource, /scenes/' + id + ', not available', '/scenes/' + str(id))


@app.route('/api/<appid>/scenes/<sid>/lightstates/<lid>', methods=['PUT'])
def apiAppScenesSceneLightstatesLight(appid, sid, lid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/scenes/' + sid + '/lightstates/' + lid)
    try:
        requestData = json.loads(request.get_data().decode())
        print(json.dumps(requestData))
        resp = globals.scenes[sid].updateLightstate(lid, requestData)
        return json_response(resp)
    except JSONDecodeError:
        return error(2, 'body contains invalid JSON', '/scenes/' + sid + '/lightstates/' + lid)


@app.route('/api/<appid>/sensors', methods=['GET'])
def apiAppSensors(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/info/timezones')
    resp = {}
    return json_response(resp)


@app.route('/api/<appid>/info/timezones', methods=['GET'])
def apiAppInfoTimezones(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/info/timezones')
    resp = []
    resp.append("Europe/Berlin")
    return json_response(resp)

@app.route('/api/<appid>/capabilities', methods=['GET'])
def apiAppCapabilities(appid):
    if not authorized(appid):
        return error(1, 'unauthorized user', '/capabilities')
    resp = {}
    resp['lights'] = {}
    resp['lights']['available'] = 63 - len(globals.lights)
    resp['timezones'] = {}
    resp['timezones']['values'] = globals.timezones
    return json_response(resp)


@app.route('/index.html', methods=['GET'])
def guiIndex():
    if not 'key' in request.args:
        return render_template('login.html')
    if not authorized(request.args['key']):
        return render_template('login.html', key=request.args['key'], error='Invalid key. Please try again.')
    return render_template('index.html', globals=globals, key=request.args['key'])


@app.route('/lights.html')
def guiLights():
    if not 'key' in request.args:
        return render_template('login.html')
    if not authorized(request.args['key']):
        return render_template('login.html', key=request.args['key'], error='Invalid key. Please try again.')
    return render_template('lights.html', globals=globals, key=request.args['key'])


@app.route('/config.html')
def guiConfig():
    if not 'key' in request.args:
        return render_template('login.html')
    if not authorized(request.args['key']):
        return render_template('login.html', key=request.args['key'], error='Invalid key. Please try again.')
    return render_template('config.html', globals=globals, key=request.args['key'])


@app.route('/login.html')
def guiLogin():
    return render_template('login.html')


@app.errorhandler(405)
def page_not_found(e):
    try:
        path = request.path[5:]
        resource = path[path.index('/'):]
        return error(4, 'method, ' + request.method + ', not available for resource, ' + resource, resource)
    except ValueError:
        return error(4, 'method, ' + request.method + ', not available for resource, /', '/')


def error(id, desc, addr):
    print('Error: ' + desc + ' (at ' + addr + ')')
    resp = [{}]
    resp[0]['error'] = {}
    resp[0]['error']['address'] = addr
    resp[0]['error']['description'] = desc
    resp[0]['error']['type'] = str(id)
    return json_response(resp)


def json_response(resp):
    respStr = json.dumps(resp)
    #    print('Response: ' + respStr)
    return Response(respStr, mimetype='application/json')


def upnp_send():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    while True:
        sock.sendto(('NOTIFY * HTTP/1.1\r\n'
                     'HOST: 239.255.255.250:1900\r\n'
                     'CACHE-CONTROL: max-age=100\r\n'
                     'LOCATION: http://' + util.getIp() + '/description.xml\r\n'
                                                          'SERVER: FreeRTOS/6.0.5, UPnP/1.0, IpBridge/0.1\r\n'
                                                          'NTS: ssdp:alive\r\n'
                                                          'NT: uuid:0FDD7736-722C-4995-89F2-ABCDEF000000\r\n'
                                                          'USN: uuid:0FDD7736-722C-4995-89F2-ABCDEF000000').encode(),
                    ('239.255.255.250', 1900))
        sleep(20)


def upnp_recv():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP,
                    socket.inet_aton('239.255.255.250') + socket.inet_aton(util.getIp()))
    sock.bind((util.getIp(), 1900))
    while True:
        data, addr = sock.recvfrom(1024)
        if 'M-SEARCH' in data:
            print('received M-SEARCH from ", addr, "\n", data')
            outSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            outSock.sendto(('HTTP/1.1 OK\r\n'
                            'CACHE-CONTROL: max-age=100\r\n'
                            'EXT:\r\n'
                            'LOCATION: http://' + util.getIp() + '/description.xml\r\n'
                                                                 'SERVER: FreeRTOS/6.0.5, UPnP/1.0, IpBridge/0.1\r\n'
                                                                 'ST: uuid:0FDD7736-722C-4995-89F2-ABCDEF000000\r\n'
                                                                 'USN: uuid:0FDD7736-722C-4995-89F2-ABCDEF000000').encode(),
                           addr)
            outSock.close()
            print("Response sent")


if __name__ == '__main__':
    globals.init()
    loadApplications()
    globals.applications[globals.config.superuser_id] = Application(globals.config.superuser_id, "superuser")
    log = logging.getLogger('werkzeug')
    # log.setLevel(logging.ERROR)
    # Thread(target=upnp_send).start()
    # Thread(target=upnp_recv).start()
    app.run(debug=True, port=80, host=util.getIp())
