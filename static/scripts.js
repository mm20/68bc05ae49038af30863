function putRequest(url, data, callback) {
    var json = JSON.stringify(data);
    var xhr = new XMLHttpRequest();
     xhr.onreadystatechange = function () {
        callback(xhr.response);
    };
    xhr.open('PUT', url, true);
    xhr.send(json)
}

function getRequest(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        callback(xhr.response);
    };
    xhr.open('GET', url, true);
    xhr.send(null)
}