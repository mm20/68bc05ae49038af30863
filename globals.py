import pigpio
import pytz

from config import Config
from group import Group
from light import Light

lights = None
groups = None
applications = None
scenes = None
config = None
timezones = None


def init():
    global lights
    lights = {}
    global pi
    pi = pigpio.pi()
    lights['1'] = Light(1, 17, 27, 22, pi, gamma=1.8, name='Desk')
    lights['2'] = Light(2, 20, 16, 21, pi, gamma=1.8, name='Bed')
    lights['3'] = Light(3, 13, 19, 26, pi, gamma=1.8, name='Ambiance')
    global groups
    groups = {}
    groups['0'] = Group(0, ['1', '2', '3'], name='All lights')
    groups['1'] = Group(1, ['1', '2'], name='Room', class_='Other')
    groups['2'] = Group(2, ['1'], name='Desk', class_='Office')
    groups['3'] = Group(3, ['2'], name='Bed', class_='Bedroom')
    groups['4'] = Group(4, ['3'], name='Ambiance', class_='Bedroom')
    global applications
    applications = {}
    global scenes
    scenes = {}
    global config
    config = Config()
    global timezones
    timezones = pytz.all_timezones
