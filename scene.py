import json
from datetime import datetime

import pytz

import globals
import util


class Scene:
    def __init__(self, id, name, lights, owner, appdata='', appdatav=1, recycle=False, locked=False, picture='',
                 version=2, transtime=4):
        self.id = id
        self.name = name
        self.owner = owner
        self.recycle = recycle
        self.locked = locked
        self.appdata = appdata
        self.appdata_version = appdatav
        self.picture = picture
        self.last_updated = util.timeToStr(datetime.now(tz=pytz.UTC))
        self.version = version
        self.transitiontime = transtime
        self.states = {}
        for l in lights:
            lamp = globals.lights[l]
            light = {}
            light['mode'] = lamp.mode
            if lamp.mode == 'xy':
                light['xy'] = lamp.getXY()
            elif lamp.mode == 'ct':
                light['ct'] = lamp.ct
            elif lamp.mode == 'hs':
                light['hue'] = lamp.hue
                light['sat'] = lamp.sat
            light['bri'] = lamp.bri
            light['on'] = lamp.state
            light['effect'] = lamp.effect
            light['transitiontime'] = self.transitiontime
            self.states[l] = light

    def pack(self):
        resp = {}
        resp['name'] = self.name
        resp['lights'] = []
        for k, v in self.states.items():
            resp['lights'].append(k)
        resp['owner'] = self.owner
        resp['recycle'] = self.recycle
        resp['locked'] = self.locked
        resp['appdata'] = {}
        resp['appdata']['version'] = self.appdata_version
        resp['appdata']['data'] = self.appdata
        resp['picture'] = self.picture
        resp['lastupdated'] = self.last_updated
        resp['version'] = self.version
        resp['lightstates'] = {}
        for k, v in self.states.items():
            resp['lightstates'][k] = {}
            resp['lightstates'][k]['on'] = v['on']
            if v['mode'] == 'xy':
                resp['lightstates'][k]['xy'] = v['xy']
            elif l['mode'] == 'ct':
                resp['lightstates'][k]['ct'] = v['ct']
            elif l['mode'] == 'hs':
                resp['lightstates'][k]['hue'] = v['hue']
                resp['lightstates'][k]['sat'] = v['sat']
            resp['lightstates'][k]['bri'] = v['bri']
            resp['lightstates'][k]['transistiontime'] = v['transitiontime']
            resp['lightstates'][k]['effect'] = v['effect']
        return resp

    def apply(self):
        for l in self.states:
            lamp = globals.lights[l['light']]
            if l['mode'] == 'xy':
                lamp.setXY(l['xy'])
            elif l['mode'] == 'ct':
                lamp.setCT(l['ct'])
            elif l['mode'] == 'hs':
                lamp.setHue(l['hue'])
                lamp.setSat(l['sat'])
            lamp.setBri(l['bri'])
            lamp.setState(l['on'])
            lamp.updateColor(self.transitiontime)

    def updateLightstate(self, light, state):
        resp = []
        if 'on' in state:
            self.states[light]['state'] = state['on']
        if 'bri' in state:
            self.states[light]['bri'] = state['bri']
        if 'hue' in state:
            self.states[light]['hue'] = state['hue']
        if 'sat' in state:
            self.states[light]['sat'] = state['sat']
        if 'xy' in state:
            self.states[light]['xy'] = state['xy']
        if 'ct' in state:
            self.states[light]['ct'] = state['ct']
        if 'effect' in state:
            self.states[light]['effect'] = state['effect']
        if 'transitiontime' in state:
            self.states[light]['transitiontime'] = state['transitiontime']
        for k, v in state.items():
            r = {'success': {}}
            r['success']['address'] = '/scenes/' + self.id + '/lightstates/' + light + '/' + k
            r['success']['value'] = v
            resp.append(r)
        print(json.dumps(resp))
        return resp

    def recall(self):
        for k,v in self.states:
            if k in globals.lights:
                globals.lights[k].apply(v, {})