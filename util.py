import datetime
import socket
import uuid


def timeToStr(time: datetime):
    return time.strftime('%Y-%m-%dT%H:%M:%S')



def getMacAddress():
    mac_num = hex(uuid.getnode()).replace('0x', '').lower()
    mac = ':'.join(mac_num[i: i + 2] for i in range(0, 11, 2))
    return mac

def getId():
    id = hex(uuid.getnode()).replace('0x', '').lower()
    return str(id)

def getIp():
    return socket.gethostbyname(socket.gethostname())
