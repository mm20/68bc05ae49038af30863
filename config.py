from datetime import datetime
from pathlib import Path

import os
import pytz

import globals
import util
from application import Application


class Config:
    def __init__(self):
        self.name = 'Philips Hue'
        self.apiversion = '1.23'
        self.linkbutton = True
        self.netmask = '225.225.225.0'
        self.gateway = '192.168.2.1'
        self.dhcp = True
        self.timezone = 'Europe/Berlin'
        self.model = '2nd Gen Bridge'
        self.modelid = 'BSB002'
        self.bridgeid = util.getId()
        self.config_dir = '/home/alarm/'
        self.swversion = '1711151408'
        self.emulator_version = '0.1'
        self.ip = util.getIp()
        self.mac = util.getMacAddress()
        self.superuser_id = self.get_superuser()

    def pack(self):
        resp = {}
        resp['name'] = self.name
        ### deprecated in 1.20
        resp['swupdate'] = {}
        resp['swupdate']['checkforupdate'] = False
        resp['swupdate']['updatestate'] = 0
        resp['swupdate']['notify'] = False
        resp['swupdate']['url'] = 'meethue.com'
        resp['swupdate']['text'] = ''
        ###
        resp['swupdate2'] = {}
        resp['swupdate2']['bridge'] = {}
        resp['swupdate2']['bridge']['state'] = '2017-11-04T23:00:00'
        resp['swupdate2']['bridge']['lastinstall'] = 'noupdates'
        resp['swupdate2']['checkforupdate'] = False
        resp['swupdate2']['state'] = 'noupdates'
        resp['swupdate2']['install'] = False
        resp['swupdate2']['autoinstall'] = {}
        resp['swupdate2']['autoinstall']['on'] = False
        resp['swupdate2']['autoinstall']['updatetime'] = 'T14:00:00'
        resp['swupdate2']['lastchange'] = '2017-11-04T23:00:00'
        resp['internetservices'] = {}
        resp['internetservices']['internet'] = 'Connected'
        resp['internetservices']['remoteaccess'] = 'Disconnected'
        resp['internetservices']['time'] = 'Connected'
        resp['internetservices']['swupdate'] = 'Connected'
        resp['backup'] = {}
        resp['backup']['status'] = 'idle'
        resp['backup']['errorcode'] = 0
        resp['whitelist'] = {}
        for k,v in globals.applications.items():
            resp['whitelist'][k] = v.pack()
        resp['portalstate'] = {}
        resp['portalstate']['signedon'] = False
        resp['portalstate']['incoming'] = False
        resp['portalstate']['outgoing'] = False
        resp['portalstate']['communication'] = 'disconnected'
        resp['apiversion'] = self.apiversion
        resp['swversion'] = self.swversion
        resp['linkbutton'] = self.linkbutton
        resp['ipaddress'] = util.getIp()
        resp['mac'] = util.getMacAddress()
        resp['netmask'] = self.netmask
        resp['gateway'] = self.gateway
        resp['dhcp'] = self.dhcp
        resp['portalservices'] = False
        resp['UTC'] = util.timeToStr(datetime.now(tz=pytz.UTC))
        resp['localtime'] = util.timeToStr(datetime.now())
        resp['timezone'] = self.timezone
        resp['zigbeechannel'] = '11'
        resp['modelid'] = self.modelid
        resp['bridgeid'] = self.bridgeid
        resp['factorynew'] = False
        resp['replacedbridgeid'] = None
        resp['datastoreversion'] = 1
        resp['starterkitid'] = None
        return resp

    def get_superuser(self):
        file = Path(self.config_dir + "superuser")
        if file.is_file():
            with open(self.config_dir + 'superuser', 'r') as sufile:
                su = sufile.read()
            return su
        else:
            key = bytearray(os.urandom(15)).hex()
            with open(self.config_dir + "superuser", "w") as text_file:
                text_file.write(key)
            return key