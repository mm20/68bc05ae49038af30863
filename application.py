from datetime import datetime

import pytz

import util


class Application:
    def __init__(self, username, devicetype):
        self.username = username
        self.devicetype = devicetype
        self.created = util.timeToStr(datetime.now(tz=pytz.UTC))
        self.last_use = util.timeToStr(datetime.now(tz=pytz.UTC))

    def pack(self):
        resp = {}
        resp['last use date'] = self.last_use
        resp['create date'] = self.created
        resp['name'] = self.devicetype
        return resp
